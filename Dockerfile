FROM node:lts as build

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
RUN npm ci --only-production
COPY tsconfig.json .
COPY ./public ./public
COPY ./src ./src

RUN npm run build

FROM nginx:stable as run

COPY --from=build /usr/src/app/build /var/www/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
