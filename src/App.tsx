import React, { useState } from "react";
import logo from "./logo.svg";
import "./App.css";

const num_cell = 13;

class Node {
  value = 0;
  isBorder = false;
  constructor(isBorder = false, value = 0) {
    this.value = value;
    this.isBorder = isBorder;
  }
}

function calculate_value_on_wall(
  wall_1: number,
  wall_2: number,
  middle: number
) {
  return (wall_1 + wall_2 + 2 * middle) / 4;
}

function calculate_value_middle(
  value_1: number,
  value_2: number,
  value_3: number,
  value_4: number
) {
  return (value_1 + value_2 + value_3 + value_4) / 4;
}

function calculate_value_corner(value_1: number, value_2: number) {
  return (value_1 + value_2) / 2;
}

function App() {
  let cells = [
    [
      new Node(),
      new Node(),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(),
      new Node(),
    ],
    [
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(true, -3.2),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(true, 3.2),
    ],
    [
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
      new Node(),
    ],
    [
      new Node(),
      new Node(),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(true),
      new Node(),
      new Node(),
    ],
  ];
  const [max_recursion_level, set_max_recursion_level] = useState(0);
  for (let _ = 0; _ < max_recursion_level; ++_) {
    // corners
    // top left
    cells[0][0].value = calculate_value_corner(
      cells[1][0].value,
      cells[0][1].value
    );
    // bottom left
    cells[12][0].value = calculate_value_corner(
      cells[11][0].value,
      cells[12][1].value
    );
    // bottom right
    cells[12][12].value = calculate_value_corner(
      cells[11][12].value,
      cells[12][11].value
    );
    // top right
    cells[0][12].value = calculate_value_corner(
      cells[1][12].value,
      cells[0][11].value
    );
    // walls
    // top walls
    cells[0][1].value = calculate_value_on_wall(
      cells[0][0].value,
      cells[0][2].value,
      cells[1][1].value
    );
    cells[0][11].value = calculate_value_on_wall(
      cells[0][10].value,
      cells[0][12].value,
      cells[1][11].value
    );
    // left walls
    cells[1][0].value = calculate_value_on_wall(
      cells[0][0].value,
      cells[2][0].value,
      cells[1][1].value
    );
    cells[11][0].value = calculate_value_on_wall(
      cells[10][0].value,
      cells[12][0].value,
      cells[11][1].value
    );
    // bottom walls
    cells[12][1].value = calculate_value_on_wall(
      cells[12][0].value,
      cells[12][2].value,
      cells[11][1].value
    );
    cells[12][11].value = calculate_value_on_wall(
      cells[12][10].value,
      cells[12][12].value,
      cells[11][11].value
    );
    // right walls
    cells[1][12].value = calculate_value_on_wall(
      cells[0][12].value,
      cells[2][12].value,
      cells[1][11].value
    );
    cells[11][12].value = calculate_value_on_wall(
      cells[10][12].value,
      cells[12][12].value,
      cells[11][11].value
    );

    for (let i = 1; i < 12; ++i) {
      for (let j = 1; j < 12; ++j) {
        cells[i][j].value = calculate_value_middle(
          cells[i + 1][j].value,
          cells[i][j + 1].value,
          cells[i - 1][j].value,
          cells[i][j - 1].value
        );
      }
    }
  }
  return (
    <>
      <div>
        <span>Niveau de récursion: </span>
        <input
          type="number"
          value={max_recursion_level}
          onChange={(e) => {
            set_max_recursion_level(Number(e.target.value));
          }}
        />
      </div>
      <table>
        {cells.map((row, i) => (
          <tr>
            {row.map((cell, j) => {
              let background = "pink";
              const is_wall = i === 0 || i === 12 || j === 0 || j === 12;
              const is_corner = (i === 0 || i === 12) && (j === 0 || j === 12);
              let background_color = "white";
              if (cell.isBorder) {
                background_color = "rgb(152,152,152)";
              }
              if (is_corner) {
                let angle = 135;
                if (i === 0 && j === 12) {
                  angle += 90;
                } else if (i === 12 && j === 0) {
                  angle += 270;
                } else if (i === 12 && j === 12) {
                  angle += 180;
                }
                background =
                  "linear-gradient(" +
                  (angle + 45) +
                  "deg, white 50%, transparent 50%), \
                  linear-gradient(" +
                  (angle - 45) +
                  "deg, white 50%, pink 50%)";
              } else if (is_wall) {
                let angle = 180;
                if (j === 0) angle = 90;
                else if (j === 12) angle = 270;
                else if (i === 12) angle = 0;
                background =
                  "linear-gradient(" +
                  angle +
                  "deg, " +
                  background_color +
                  " 50%, pink 50%)";
              }
              return (
                <td
                  style={{
                    background,
                  }}
                >
                  {(Math.round(cell.value * 100000) / 100000).toPrecision(3)} V
                </td>
              );
            })}
          </tr>
        ))}
      </table>
    </>
  );
}

export default App;
